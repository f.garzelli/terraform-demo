provider "google" {

}

resource "google_storage_bucket" "my_bucket" {
  project       = "<your project id here>"
  name          = "<globally-unique name of the bucket to be created>"
  location      = "europe-west4"
}
