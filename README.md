# Demo 1

## Install Terraform

```bash
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common

# Install the HashiCorp GPG key.
wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

gpg --no-default-keyring \
--keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
--fingerprint

# Add the official HashiCorp repository to your system.
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list

sudo apt update
sudo apt-get install terraform

# Verify installation
terraform -help
```

## Your First Configuration

Clone repo:

```bash
git clone https://gitlab.com/f.garzelli/terraform-demo.git
cd terraform-demo
```

Authenticate to Google Cloud:

1. `gcloud auth application-default login`
2. click on the link
3. copy the secret code
4. paste it

![](img/adc.png)

Edit main.tf, specifying project ID and bucket name.

```bash
terraform init
terraform plan
terraform apply
```
